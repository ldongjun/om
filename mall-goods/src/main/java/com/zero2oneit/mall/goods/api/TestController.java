package com.zero2oneit.mall.goods.api;

import com.zero2oneit.mall.common.utils.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/3/31
 */
@RestController
@RequestMapping("/api/user")
public class TestController {

    @RequestMapping("/login")
    public R login(String userName, String password){
        System.out.println("请求进来啦 :" +userName +"-----------" + password);
        return R.ok();
    }

}
