package com.zero2oneit.mall.search.service;


import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.search.qo.IndexSearchQo;
import com.zero2oneit.mall.search.qo.ShopTypeGoodQo;

import java.util.List;
import java.util.Map;

/**
 * @author Lee
 * @date 2021/3/17 -15:29
 */
public interface GoodProEsService {

    Map getByIndexAndId(String index,String id);

    List productSkuByProId(String id);

    Map skuListStock(List<String> list);

    R keywordSearch(Map<String,String> map);

    BoostrapDataGrid myProSearch(Map<String, String> map, String index);

    BoostrapDataGrid businessSearch(Map<String, String> map);

    BoostrapDataGrid indexProList(IndexSearchQo qo);

    R yzBusinessTypeSearch(ShopTypeGoodQo qo);

    BoostrapDataGrid yzProSearch(Map<String, String> map);

    BoostrapDataGrid businessType(ShopTypeGoodQo qo);

    Map hotGood();

    BoostrapDataGrid sortSearch(Map<String, String> map);

    Map yzProductSku(String skuIndex,String proIndex,String id, String productId);

    List<Map> purchasedSku(List<Map> list);

}
