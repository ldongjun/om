/**
 * 提示与加载工具类
 */
export default class Tips {
  
  constructor() {}
  
  /**
   * 请求成功，跳转
   * res : 对象
   * duration ：延迟时间（单位毫秒）
   * url：跳转路径
   */
  static success(res, duration, url) {
       res.msg ? uni.showToast({ title: res.msg, icon: "none"}): ''
	   if(res.success && url){
		   setTimeout(()=>{
			   uni.navigateTo({url})
		   },duration)
	   }else if(!res.success){
		   return
	   }
  }
  
  /**
   * 请求成功，返回上某层
   * delta：返回的页面数
   */
  static successBack(res, duration, delta) {
	   uni.showToast({ title: res.msg, icon: "none"})
	   if(res.success){
	   		   setTimeout(()=>{
	   			   uni.navigateBack({
	   			   	delta
	   			   })
	   		   },duration)
	   }
  }
  
  static successRedirect({res, duration, url,msg,fn}) {
  	   if(res.success){
		   msg ? uni.showToast({ title: msg, icon: "none"}): ''
		   fn && fn()
  		   if(url){
			   setTimeout(()=>{
			     	uni.redirectTo({url})
			   },duration)
		   }
  	   }else if(!res.success){
		   res.msg ? uni.showToast({ title: res.msg, icon: "none"}): ''
  		   return
  	   }
  }
  
  /**
    * 普通提示
    * icon: 参数有 'loading', 'none','success'
    */
   static toast(title,icon='none') {
  		uni.showToast({
  		  title: title,
  		  icon,
  		  mask: true,
  		  duration:1000
  		});
   }
   
}
