package com.zero2oneit.mall.market.api;

import com.zero2oneit.mall.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/4/24
 */
@RestController
@RequestMapping("/api/advert")
public class AdvertApi {

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 加载首页广告图片
     * @return
     */
    @PostMapping("/load")
    public R load(){
        String list = (String) redisTemplate.opsForHash().get("om:market:advert", "A5");
        System.out.println("list = " + list);
        return R.ok("加载成功", list);
    }

}
